declare module "boid" {
  class Vec2 {
    static get(x: number, y: number): Vec2;
    static fill(number);
    static angleBetween(a: Vec2, b: Vec2): number;

    constructor(x: number, y: number);
    add(vec: Vec2): Vec2;
    subtract(vec: Vec2): Vec2;
    normalize(): Vec2;
    isNormalized(): boolean;
    truncate(max: number): Vec2;
    scaleBy(mul: number): Vec2;
    divideBy(div: number): Vec2;
    equals(vec: Vec2): Vec2;
    negate(): Vec2;
    dotProduct(vec: Vec2): number;
    crossProduct(vec: Vec2): number;
    distanceSq(vec: Vec2): number;
    distance(vec: Vec2): number;
    clone(): Vec2;
    reset(): Vec2;
    copy(vec: Vec2): Vec2;
    perpendicular(): Vec2;
    sign(vec: Vec2): number;
    set(angle: number, length: number): Vec2;
    dispose();

    readonly lengthSquared: number;
    length: number;
    angle: number;
    x: number;
    y: number;
  }

  type Obstacle = {
    radius: number;
    position: Vec2;
  };

  type BoidOptions = {
    bounds?: {
      x?: number;
      y?: number;
      width?: number;
      height?: number;
    };
    edgeBehavior?: "none" | "bounce" | "wrap";
    mass?: number;
    maxSpeed?: number;
    maxForce?: number;
    radius?: number;
    arriveThreshold?: number;
    wanderDistance?: number;
    wanderRadius?: number;
    wanderAngle?: number;
    wanderRange?: number;
    avoidDistance?: number;
    pathThreshold?: number;
    maxDistance?: number;
    minDistance?: number;
  };

  export default class Boid {
    static EDGE_NONE = "none";
    static EDGE_BOUNCE = "bounce";
    static EDGE_WRAP = "wrap";

    static vec2(x: number, y: number): Vec2;
    static obstacle(radius: number, x: number, y: number): Obstacle;

    constructor(options: BoidOptions);
    setBounds(width: number, height: number, x: number, y: number): Boid;
    seek(targetVec: Vec2): Boid;
    flee(targetVec: Vec2): Boid;
    arrive(targetVec: Vec2): Boid;
    pursue(targetVec: Vec2): Boid;
    evade(targetVec: Vec2): Boid;
    wander(): Boid;
    avoid(obstacles: Obstacle[]): Boid;
    followPath(path: Vec2[], loop?: boolean): Boid;
    flock(boids: Boid[]): Boid;
    update(): Boid;

    position: Vec2;
    velocity: Vec2;
    userData: any;
    edgeBehavior: "none" | "bounce" | "wrap";
    mass: number;
    maxSpeed: number;
    maxForce: number;
    radius: number;
    arriveThreshold: number;
    wanderDistance: number;
    wanderRadius: number;
    wanderAngle: number;
    wanderRange: number;
    avoidDistance: number;
    pathIndex: number;
    pathThreshold: number;
    maxDistance: number;
    minDistance: number;
  }
}
