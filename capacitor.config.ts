import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'fishtank.bhdouglass',
  appName: 'Fish Tank',
  webDir: 'dist',
  bundledWebRuntime: false
};

export default config;
