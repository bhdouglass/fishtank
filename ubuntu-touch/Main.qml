/*
 * Copyright (C) 2022  Brian Douglass
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * appname is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
import io.thp.pyotherside 1.4
import QtWebEngine 1.7
import Morph.Web 0.1

MainView {
    id: root
    objectName: "mainView"
    applicationName: "fishtank.bhdouglass"
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    property bool loading: true

    WebContext {
        id: webcontext
        offTheRecord: false
    }

    ActivityIndicator {
        visible: loading
        running: loading
        anchors.centerIn: parent
    }

    WebView {
        id: webview
        visible: !loading
        anchors.fill: parent

        context: webcontext
        url: "http://127.0.0.1:6294"

        function navigationRequestedDelegate(request) {
            var url = request.url.toString();

            if (!url.match("(http|https)://127.0.0.1:6294/(.*)") && request.isMainFrame) {
                Qt.openUrlExternally(url);
                request.action = WebEngineNavigationRequest.IgnoreRequest;
            }
        }
    }

    Python {
        id: python

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl("."));

            importModule("server", function() {
                console.log("server imported");
                python.call("server.run", [], function() {
                    console.log("running server");
                    webview.reload();
                    loading = false;
                });
            });
        }

        onError: {
            console.log("python error: " + traceback);
        }
    }
}
