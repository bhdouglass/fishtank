import mimetypes


# /etc/mime.types is not readable by the app, so we need to initialize it before http.server does
mimetypes.knownfiles = []
mimetypes.init(files=['./mime.types'])

from http.server import HTTPServer, SimpleHTTPRequestHandler
import sys
from threading import Thread


class Handler(SimpleHTTPRequestHandler):
    def parse_request(self):
        res = super().parse_request()

        # Specifying the directory to serve is not available in the ubuntu touch python version (3.5 at this time)
        self.path = '/www' + self.path

        return res


def serve():
    httpd = HTTPServer(("127.0.0.1", 6294), Handler)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        sys.exit(0)


def run():
    t = Thread(target=serve)
    t.start()
