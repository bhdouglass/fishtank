#!/bin/bash

set -e

mkdir -p $SRC_DIR/assets
cp $ROOT/public/logo.svg $SRC_DIR/assets/

rm -rf $SRC_DIR/www
mkdir -p $SRC_DIR/www
npm ci --cache .npm --prefer-offline
npm run build
cp -r $ROOT/dist/* $SRC_DIR/www

rm -rf $INSTALL_DIR/*
cp -r $SRC_DIR/* $INSTALL_DIR/

rm -f $INSTALL_DIR/build.sh

# TODO reduce click size?
#rm -f $INSTALL_DIR/www/assets/*.mp3
