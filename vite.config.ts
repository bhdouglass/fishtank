import { fileURLToPath, URL } from "node:url";

import { defineConfig } from "vite";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
  build: {
    // phaser doesn't accept inlined assets: https://github.com/Sun0fABeach/vue-phaser-vite/blob/master/vite.config.ts#L14
    assetsInlineLimit: 0,
  },
});
