import { Scene, GameObjects, Math as PhaserMath } from "phaser";
import Boid from "boid";
import type { SpritesheetKey } from "./types";
import spritesheetRef from "./assets/img/fish-spritesheet-ref.json";

export const FISH_OPTIONS: SpritesheetKey[] = [
  // eslint-disable-next-line prettier/prettier
  "fishGreenOutline", 'fishPurpleOutline', 'fishBlueOutline', 'fishRedOutline', 'fishOrangeOutline', 'fishPufferOutline', 'fishEelOutline'
];

const SPEED: { [key: string]: number } = {
  fishGreenOutline: 0.4,
  fishPurpleOutline: 0.7,
  fishBlueOutline: 0.5,
  fishRedOutline: 0.4,
  fishOrangeOutline: 0.8,
  fishPufferOutline: 0.2,
  fishEelOutline: 0.3,
};

export type Point = { x: number; y: number };
export type Bounds = { width: number; height: number };

enum FACING {
  LEFT = "left",
  RIGHT = "right",
}

const WIGGLE_FACTOR = 20;

export enum BEHAVIOR {
  WANDER = "wander",
  SEEK = "seek",
  FLEE = "flee",
}

// TODO add hunger, make the fish go seek out algea
export class Fish {
  scene: Scene;
  boid: Boid;
  sprite: GameObjects.Sprite;
  facing: FACING;
  wiggle: number = 0; // Prevent the fish from rapidly switching facings
  hunger: number = 100;
  hungerSpeed: number = 2;
  foodTarget?: Point;

  constructor(scene: Scene, group: GameObjects.Group, origin: Point, bounds: Bounds, spritesheetKey?: SpritesheetKey) {
    this.scene = scene;

    if (!spritesheetKey) {
      spritesheetKey = PhaserMath.RND.pick(FISH_OPTIONS);
    }

    const x = PhaserMath.RND.integerInRange(0, bounds.width);
    const y = PhaserMath.RND.integerInRange(0, bounds.height);
    this.sprite = group.create(x, y, "spritesheet", spritesheetRef[spritesheetKey]);
    this.sprite.setOrigin(0.5, 0.5);

    const speed = SPEED[spritesheetKey] ?? 0.5;
    const maxSpeed = PhaserMath.RND.realInRange(speed - 0.1, speed + 0.1);

    this.boid = new Boid({
      wanderDistance: 20,
      wanderRadius: 1,
      wanderAngle: 0,
      wanderRange: 1,
      bounds: {
        x: origin.x,
        y: origin.y,
        width: bounds.width,
        height: bounds.height,
      },
      maxSpeed,
    });
    this.boid.position.x = x;
    this.boid.position.y = y;

    this.facing = FACING.RIGHT;
    if (this.boid.velocity.x < 0) {
      this.facing = FACING.LEFT;
      this.sprite.scaleX *= -1;
    }

    this.hungerSpeed = 0.25 * maxSpeed;
    this.hunger = PhaserMath.RND.integerInRange(100, 300);
  }

  private hungerUpdate(foodOptions: Point[]) {
    if (!this.foodTarget) {
      this.foodTarget = PhaserMath.RND.pick(foodOptions);
    }

    this.boid.arrive(Boid.vec2(this.foodTarget.x, this.foodTarget.y));

    const distance = PhaserMath.Distance.BetweenPoints(this.foodTarget, this.sprite);
    if (distance < 50) {
      this.hunger = PhaserMath.RND.integerInRange(400, 600);
      this.foodTarget = undefined;
    }
  }

  // TODO add a flocking behavior based on type
  // TODO tap a fish to make them flee at higher speed
  update(behavior: BEHAVIOR, target: Point, foodOptions: Point[]) {
    this.hunger -= this.hungerSpeed;

    if (behavior == BEHAVIOR.WANDER) {
      if (this.hunger <= 0) {
        this.hungerUpdate(foodOptions);
      } else {
        this.boid.wander();
      }
    } else if (behavior == BEHAVIOR.SEEK) {
      this.boid.arrive(Boid.vec2(target.x, target.y));
    } else if (behavior == BEHAVIOR.FLEE) {
      this.boid.flee(Boid.vec2(target.x, target.y));
    }

    this.boid.update();

    const dx = this.boid.position.x - this.sprite.x;
    const dy = this.boid.position.y - this.sprite.y;
    const rotation = Math.atan2(dy, dx);

    let facing = FACING.RIGHT;
    if (rotation > 0.5 * Math.PI || rotation < -0.5 * Math.PI) {
      facing = FACING.LEFT;
    }

    if (facing != this.facing) {
      if (this.wiggle > WIGGLE_FACTOR) {
        this.wiggle = 0;

        this.facing = facing;
        this.sprite.scaleX *= -1;
      } else {
        this.wiggle++;
      }
    }

    this.sprite.x = this.boid.position.x;
    this.sprite.y = this.boid.position.y;
  }
}
