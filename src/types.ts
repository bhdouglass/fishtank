import type spritesheetRef from "./assets/img/fish-spritesheet-ref.json";

export type SpritesheetKey = keyof typeof spritesheetRef;
