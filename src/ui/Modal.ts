import { type Scene, Geom, type GameObjects, type Textures } from "phaser";

import spritesheetRef from "../assets/img/fish-spritesheet-ref.json";

const BORDER = 3;

export class Modal {
  private sprite: GameObjects.Sprite;
  private closeSprite: GameObjects.Sprite;
  innerBoundingBox: Geom.Rectangle;
  private colors = {
    border: 0x999999,
    insideBorder: 0xffffff,
    inside: 0xeeeeee,
  };

  get textureKey() {
    return `${this.name}-modal`;
  }

  constructor(
    private scene: Scene,
    private name: string,
    private center: Geom.Point,
    private width: number,
    private height: number,
    private onClose?: Function,
  ) {
    this.draw();

    this.innerBoundingBox = new Geom.Rectangle(
      this.center.x - width / 2 + BORDER * 3,
      this.center.y - height / 2 + 32,
      width - BORDER * 6,
      height - 32 - BORDER * 3,
    );

    this.sprite = this.scene.add.sprite(this.center.x, this.center.y, this.textureKey);
    this.closeSprite = this.scene.add.sprite(
      this.center.x + width / 2 - 20,
      this.center.y - height / 2 + 20,
      "spritesheet",
      spritesheetRef.x,
    );
    this.closeSprite.setInteractive();

    this.closeSprite.on("pointerup", () => {
      this.close();
    });
  }

  private draw() {
    const existing = (this.scene.textures.list as { [key: string]: Textures.Texture })[this.textureKey];
    if (existing) {
      return;
    }

    const graphics = this.scene.add.graphics();

    const x = 0;
    const y = 0;

    // Border
    graphics.fillStyle(this.colors.border);
    graphics.fillRoundedRect(x, y, this.width, this.height, 15);

    // Inside border
    graphics.fillStyle(this.colors.insideBorder);
    graphics.fillRoundedRect(x + BORDER, y + BORDER, this.width - BORDER * 2, this.height - BORDER * 2, 12);

    // Inside
    graphics.fillStyle(this.colors.inside);
    graphics.fillRoundedRect(x + BORDER * 2, y + BORDER * 2, this.width - BORDER * 4, this.height - BORDER * 4, 10);

    graphics.generateTexture(this.textureKey, this.width, this.height);
    graphics.clear();
    graphics.destroy();
  }

  addToGroup(group: GameObjects.Group) {
    group.add(this.sprite);
    group.add(this.closeSprite);
  }

  close() {
    if (this.onClose) {
      this.onClose();
    }

    this.sprite.destroy();
    this.closeSprite.destroy();
  }
}
