import type { GameObjects, Geom, Scene } from "phaser";
import spritesheetRef from "../assets/img/fish-spritesheet-ref.json";

export class Checkbox {
  private sprite: GameObjects.Sprite;
  private text: GameObjects.Text;
  private _checked = false;

  constructor(
    private scene: Scene,
    private name: string,
    private center: Geom.Point,
    private textStr: string,
    checked: boolean,
    private onClick: Function,
  ) {
    this.sprite = this.scene.add.sprite(this.center.x, this.center.y, "spritesheet", spritesheetRef.checkboxEmpty);
    this.sprite.setInteractive();
    this.text = this.scene.add.text(this.center.x + 24, this.center.y, this.textStr, {
      fontSize: "24px",
      color: "black",
      fontStyle: "bold",
    });
    this.text.setOrigin(0, 0.5);
    this.text.setInteractive();

    this.sprite.on("pointerup", () => {
      this.checked = !this.checked;
      this.onClick();
    });
    this.text.on("pointerup", () => {
      this.checked = !this.checked;
      this.onClick();
    });

    this.checked = checked;
  }

  get checked() {
    return this._checked;
  }

  set checked(checked: boolean) {
    this._checked = checked;
    this.sprite.setTexture("spritesheet", checked ? spritesheetRef.checkboxChecked : spritesheetRef.checkboxEmpty);
  }

  addToGroup(group: GameObjects.Group) {
    group.add(this.sprite);
    group.add(this.text);
  }
}
