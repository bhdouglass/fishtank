import type { Scene, Geom, GameObjects, Textures } from "phaser";

const BORDER = 3;
const DEPTH = 6;
const MARGIN = 9;

export class Button {
  private sprite: GameObjects.Sprite;
  private text: GameObjects.Text;
  private colors = {
    depth: 0x166e93,
    border: 0x1989b8,
    inside: 0x35baf3,
    gloss: 0x1ea7e1,
    text: "black",
  };

  get textureRaisedKey() {
    return `${this.name}-button-raised`;
  }

  get texturePressedKey() {
    return `${this.name}-button-pressed`;
  }

  constructor(
    private scene: Scene,
    private name: string,
    private center: Geom.Point,
    private textStr: string,
    private width: number,
    private height: number,
    private fontSize: string,
    private onClick: Function,
  ) {
    this.draw();

    this.sprite = this.scene.add.sprite(this.center.x, this.center.y, this.textureRaisedKey);
    this.sprite.setInteractive();
    this.text = this.scene.add.text(this.center.x, this.center.y, this.textStr, {
      fontSize: this.fontSize,
      color: this.colors.text,
    });
    this.text.setOrigin(0.5, 0.5);

    this.sprite.on("pointerdown", () => {
      this.text.y = center.y + DEPTH;
      this.sprite.setTexture(this.texturePressedKey);
    });

    this.sprite.on("pointerup", () => {
      this.text.y = center.y;
      this.sprite.setTexture(this.textureRaisedKey);

      this.onClick();
    });
  }

  private draw() {
    const existingRaised = (this.scene.textures.list as { [key: string]: Textures.Texture })[this.textureRaisedKey];
    const existingPressed = (this.scene.textures.list as { [key: string]: Textures.Texture })[this.texturePressedKey];
    if (existingRaised && existingPressed) {
      return;
    }

    const graphics = this.scene.add.graphics();

    // Raised

    const x = 0;
    let y = 0;

    // Depth
    graphics.fillStyle(this.colors.depth);
    graphics.fillRoundedRect(x, y + DEPTH, this.width, this.height, 15);

    // Border
    graphics.fillStyle(this.colors.border);
    graphics.fillRoundedRect(x, y, this.width, this.height, 15);

    // Inside
    graphics.fillStyle(this.colors.inside);
    graphics.fillRoundedRect(x + BORDER, y + BORDER, this.width - BORDER * 2, this.height - BORDER * 2, 12);

    // Gloss
    graphics.fillStyle(this.colors.gloss);
    graphics.fillRect(x + MARGIN, y + this.height / 2, this.width - MARGIN * 2, this.height / 2 - MARGIN);

    graphics.generateTexture(this.textureRaisedKey, this.width, this.height + DEPTH);
    graphics.clear();

    // Pressed

    y = DEPTH;

    // Border
    graphics.fillStyle(this.colors.border);
    graphics.fillRoundedRect(x, y, this.width, this.height, 15);

    // Inside
    graphics.fillStyle(this.colors.inside);
    graphics.fillRoundedRect(x + BORDER, y + BORDER, this.width - BORDER * 2, this.height - BORDER * 2, 12);

    // Gloss
    graphics.fillStyle(this.colors.gloss);
    graphics.fillRect(x + MARGIN, y + this.height / 2, this.width - MARGIN * 2, this.height / 2 - MARGIN);

    graphics.generateTexture(this.texturePressedKey, this.width, this.height + DEPTH);
    graphics.clear();
    graphics.destroy();
  }

  addToGroup(group: GameObjects.Group) {
    group.add(this.sprite);
    group.add(this.text);
  }
}
