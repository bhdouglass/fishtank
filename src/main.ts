import "./main.css";

import * as Sentry from "@sentry/browser";
import { version } from "../package.json";

if (import.meta.env.VITE_SENTRY_DSN) {
  Sentry.init({
    release: `fishtank@${version}`,
    dsn: import.meta.env.VITE_SENTRY_DSN,
    integrations: [Sentry.browserTracingIntegration()],
  });
}

import Phaser from "phaser";
import BootScene from "./scenes/BootScene";
import FishTankScene from "./scenes/FishTankScene";
import PreloadScene from "./scenes/PreloadScene";

new Phaser.Game({
  type: Phaser.CANVAS,
  width: window.innerWidth,
  height: window.innerHeight,
  parent: "game",
  backgroundColor: "#A1D6E7",
  scene: [BootScene, PreloadScene, FishTankScene],
});
