import { Button } from "@/ui/Button";
import { Checkbox } from "@/ui/Checkbox";
import { Modal } from "@/ui/Modal";
import { Scene, Math as PhaserMath, GameObjects, Sound, Input, Geom } from "phaser";
import spritesheetRef from "../assets/img/fish-spritesheet-ref.json";
import { BEHAVIOR, Fish, FISH_OPTIONS } from "../Fish";
import type { SpritesheetKey } from "../types";
import { shuffle } from "lodash";

const DIRT_OPTIONS: SpritesheetKey[] = [
  // eslint-disable-next-line prettier/prettier
  "dirtOutline01", "dirtOutline02", "dirtOutline03", "dirtOutline04", "dirtOutline05", "dirtOutline06", "dirtOutline07", "dirtOutline08",
];

const SEAWEED_OPTIONS: SpritesheetKey[] = [
  // eslint-disable-next-line prettier/prettier
  "seaweedOutline01", "seaweedOutline02", "seaweedOutline03", "seaweedOutline04", "seaweedOutline05", "seaweedOutline06",
];

const ORNAMENT_OPTIONS: SpritesheetKey[] = [
  // eslint-disable-next-line prettier/prettier
  "coralOutline01", "coralOutline02", "coralOutline03", "coralOutline04",
  // eslint-disable-next-line prettier/prettier
  "rockOutline01", "rockOutline02"
];

const ORNAMENT_OFFSET = 10; //The ground tiles have some transparency on top

function destroyChildren(group: GameObjects.Group) {
  // Can't use a for loop here as the array gets updated
  while (group.getChildren().length > 0) {
    group.getChildren()[0].destroy();
  }
}

export default class FishTankScene extends Scene {
  fishes: Fish[] = [];
  behavior = BEHAVIOR.WANDER;

  playMusic = true;
  bgMusic?: Sound.BaseSound;
  soundFx: Sound.BaseSound[] = [];
  musicCheckbox?: Checkbox;

  aboutGroup?: GameObjects.Group;
  aboutDialogVisible = false;

  foodOptions: Geom.Point[] = [];

  constructor() {
    super({ key: "FishTankScene" });
  }

  create() {
    const { width: gameWidth, height: gameHeight } = this.game.canvas;

    this.playMusic = localStorage.getItem("play-music") === "true" || !localStorage.getItem("play-music");
    this.bgMusic = this.sound.add("seashore-peace");
    if (this.playMusic) {
      this.bgMusic.play({ loop: true });
    }

    this.soundFx.push(this.sound.add("bubbles"), this.sound.add("swim"), this.sound.add("water"));
    this.time.addEvent({
      loop: true,
      delay: 2000,
      callback: () => {
        if (this.playMusic && PhaserMath.RND.integerInRange(0, 5) == 0) {
          const sound = PhaserMath.RND.pick(this.soundFx);
          sound.play({ volume: 0.5 });
        }
      },
    });

    const groundGroup = this.add.group();
    const ornamentGroup = this.add.group();
    const fishGroup = this.add.group();
    this.aboutGroup = this.add.group();

    const numTiles = Math.ceil(gameWidth / 64) + 1;

    let ornamentsArray: (SpritesheetKey | undefined)[] = new Array(numTiles);
    const numSeaweed = numTiles * (PhaserMath.RND.integerInRange(30, 40) / 100);
    const numOrnaments = numTiles * (PhaserMath.RND.integerInRange(20, 30) / 100);
    let index = 0;
    while (index < numSeaweed + numOrnaments) {
      if (index < numSeaweed) {
        ornamentsArray[index] = PhaserMath.RND.pick(SEAWEED_OPTIONS);
      } else {
        ornamentsArray[index] = PhaserMath.RND.pick(ORNAMENT_OPTIONS);
      }

      index++;
    }
    ornamentsArray = shuffle(ornamentsArray);

    for (let i = 0; i < numTiles; i++) {
      const ornamentAsset = ornamentsArray[i];
      if (ornamentAsset) {
        const ornament: GameObjects.Sprite = ornamentGroup.create(
          64 * i,
          gameHeight - 64 + ORNAMENT_OFFSET,
          "spritesheet",
          spritesheetRef[ornamentAsset],
        );
        ornament.setOrigin(1, 1);

        if (SEAWEED_OPTIONS.includes(ornamentAsset)) {
          this.foodOptions.push(new Geom.Point(ornament.x, ornament.y));
        }
      }

      const groundAsset = PhaserMath.RND.pick(DIRT_OPTIONS);
      const groundTile: GameObjects.Sprite = groundGroup.create(64 * i, gameHeight, "spritesheet", spritesheetRef[groundAsset]);
      groundTile.setOrigin(1, 1);
    }

    const origin = { x: 32, y: 32 }; // Don't let the fish hit the glass
    const bounds = {
      width: gameWidth - origin.x * 2,
      height: gameHeight - origin.y * 2 - 44, // Include room for the ground
    };

    //Ensure that there is at least one of each fish
    FISH_OPTIONS.forEach((asset) => {
      this.fishes.push(new Fish(this, fishGroup, origin, bounds, asset));
    }, this);

    for (let j = 0; j < PhaserMath.RND.integerInRange(5, 15); j++) {
      this.fishes.push(new Fish(this, fishGroup, origin, bounds));
    }

    this.musicCheckbox = new Checkbox(this, "music-checkbox", new Geom.Point(32, gameHeight - 30), "Music", this.playMusic, () => {
      this.playMusic = this.musicCheckbox!.checked;
      if (this.playMusic) {
        this.bgMusic?.play({ loop: true });
      } else {
        this.bgMusic?.stop();
        this.soundFx.forEach((sfx) => {
          sfx.stop();
        });
      }

      localStorage.setItem("play-music", this.playMusic ? "true" : "false");
    });

    new Button(this, "about-button", new Geom.Point(gameWidth - 32, gameHeight - 32), "?", 50, 50, "36px", () => {
      this.createAboutDialog();
    });

    this.input.on("pointerdown", (pointer: Input.Pointer, objectsClicked: GameObjects.GameObject[]) => {
      if (objectsClicked.length > 0) {
        return;
      }

      if (this.aboutDialogVisible) {
        this.destroyAboutDialog();
        return;
      }

      this.behavior = BEHAVIOR.SEEK;
    });
    this.input.on("pointerup", (pointer: Input.Pointer, objectsClicked: GameObjects.GameObject[]) => {
      if (objectsClicked.length > 0) {
        objectsClicked.forEach((obj) => {
          if (this.aboutGroup!.contains(obj)) {
            const link = obj.getData("link");
            if (link) {
              window.open(link, "_blank");
            }
            this.destroyAboutDialog();
          }
        });

        return;
      }

      this.behavior = BEHAVIOR.WANDER;
    });
  }

  update() {
    this.fishes.forEach((fish) => {
      fish.update(this.behavior, this.game.input.activePointer, this.foodOptions);
    }, this);
  }

  createAboutDialog() {
    this.destroyAboutDialog();

    const { width: gameWidth, height: gameHeight } = this.game.canvas;

    const spacing = 20;
    const style = { color: "blue" };

    const modal = new Modal(this, "about-modal", new Geom.Point(gameWidth / 2, gameHeight / 2), 244, 192, () => this.destroyAboutDialog());
    modal.addToGroup(this.aboutGroup!);

    const x = modal.innerBoundingBox.x;
    const y = modal.innerBoundingBox.y;

    const line1 = this.add.text(x, y, "App by Brian Douglass", style).setInteractive().setData("link", "https://bhdouglass.com/");
    this.aboutGroup!.add(line1);

    const line2 = this.add
      .text(x, line1.y + spacing, "Image Source", style)
      .setInteractive()
      .setData("link", "https://www.kenney.nl/");
    this.aboutGroup!.add(line2);

    const line3 = this.add
      .text(x, line2.y + spacing, "Music Source", style)
      .setInteractive()
      .setData("link", "http://opengameart.org/content/seashore-peace-ambiance");
    this.aboutGroup!.add(line3);

    const line4 = this.add
      .text(x, line3.y + spacing, "Sound FX Source", style)
      .setInteractive()
      .setData("link", "http://opengameart.org/content/skippy-fish-water-sound-collectio");
    this.aboutGroup!.add(line4);

    const line5 = this.add
      .text(x, line4.y + spacing, "Source Code", style)
      .setInteractive()
      .setData("link", "https://gitlab.com/bhdouglass/fishtank");
    this.aboutGroup!.add(line5);

    const line6 = this.add
      .text(x, line5.y + spacing, "Android App", style)
      .setInteractive()
      .setData("link", "https://play.google.com/store/apps/details?id=fishtank.bhdouglass");
    this.aboutGroup!.add(line6);

    const line7 = this.add
      .text(x, line6.y + spacing, "Ubuntu Touch App", style)
      .setInteractive()
      .setData("link", "https://uappexplorer.com/app/fishtank.bhdouglass");
    this.aboutGroup!.add(line7);

    this.aboutDialogVisible = true;
  }

  destroyAboutDialog() {
    destroyChildren(this.aboutGroup!);

    this.aboutDialogVisible = false;
  }
}
