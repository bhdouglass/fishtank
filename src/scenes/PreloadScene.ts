import { Scene } from "phaser";
import bubblesOgg from "../assets/audio/jcpmcdonald/bubbles.ogg";
import bubblesMp3 from "../assets/audio/jcpmcdonald/bubbles.mp3";
import swimOgg from "../assets/audio/jcpmcdonald/swim.ogg";
import swimMp3 from "../assets/audio/jcpmcdonald/swim.mp3";
import waterOgg from "../assets/audio/jcpmcdonald/water.ogg";
import waterMp3 from "../assets/audio/jcpmcdonald/water.mp3";
import seashoreOgg from "../assets/audio/socapex/seashore-peace.ogg";
import seashoreMp3 from "../assets/audio/socapex/seashore-peace.mp3";
import spritesheet from "../assets/img/fish-spritesheet.svg";

export default class PreloadScene extends Scene {
  constructor() {
    super({ key: "PreloadScene" });
  }

  preload() {
    const logo = this.add.sprite(this.game.canvas.width / 2, this.game.canvas.height / 2, "logo");
    logo.setOrigin(0.5, 0.5);

    this.load.spritesheet("spritesheet", spritesheet, {
      frameWidth: 64,
      frameHeight: 64,
      margin: 32,
      spacing: 32,
    });

    this.load.audio("bubbles", [bubblesOgg, bubblesMp3]);
    this.load.audio("swim", [swimOgg, swimMp3]);
    this.load.audio("water", [waterOgg, waterMp3]);
    this.load.audio("seashore-peace", [seashoreOgg, seashoreMp3]);
  }

  create() {
    this.scene.start("FishTankScene");
  }
}
