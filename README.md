# Fish Tank

A simple virtual fish tank built with [phaser](http://phaser.io).

Fish Tank is available on [Android](https://play.google.com/store/apps/details?id=fishtank.bhdouglass),
[Ubuntu Touch](https://uappexplorer.com/app/fishtank.bhdouglass)
and the [web](http://fishtank.bhdouglass.com/).

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/fishtank.bhdouglass)

## Credits

### Images

The fish & ui assets are from [Kenney's game asset pack](https://kenney.itch.io/kenney-game-assets-2)
and used under the [Creative Commons Zero, CC0](http://creativecommons.org/publicdomain/zero/1.0/).

### Audio

Water sounds by [jcpmcdonald](http://opengameart.org/users/jcpmcdonald) from [OpenGameArt](http://opengameart.org/content/skippy-fish-water-sound-collection).

"Seashore Peace - Ambiance" by [Philippe Groarke (Socapex)](http://opengameart.org/users/socapex) from [OpenGameArt](http://opengameart.org/content/seashore-peace-ambiance).

## License

Copyright (C) 2022  Brian Douglass

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
